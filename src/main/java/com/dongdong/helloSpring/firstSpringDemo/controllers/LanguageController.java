package com.dongdong.helloSpring.firstSpringDemo.controllers;

import com.dongdong.helloSpring.firstSpringDemo.beans.Language;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class LanguageController {

    @GetMapping("/languages")
    public List<Language> getLanguage(){
        List<Language> languageList = new ArrayList<>();
        languageList.add(new Language("English", "En"));
        languageList.add(new Language("French", "Fr"));
        languageList.add(new Language("Chinese", "Zh"));
        return languageList;
    }
}
