package com.dongdong.helloSpring.firstSpringDemo.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Language {
    private String language;
    private String abbreviation;
}
